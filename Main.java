package com.company;

import java.util.Scanner;

public class Main {

    /* Metoda do inicjalizacji planszy, initializeBoard(int n), ma inicjalizować planszę o rozmiarze n, (nie wiem co ma zwracac, char[][] lub jakis String)

       Metoda wyświetlająca zasady gry, rules(), ma pokazywać warunki wygranej/przegranej/remisu

       Metoda wyswietlajaca plansze z obecnym stanem gry showBoard(), musi byc aktualizowana po kazdym ruchu

       Gra ma umożliwić rywalizację między dwiema osobami do wyboru, ktory gracz rozpoczyna (moze dodac losowanie jak nie mozna zdecydowac?)

       Metoda, która określa koniec gry, kiedy gracz wygra lub wystąpi remis

       Metoda, ktora pobiera dane z standardowego wejscia(Scanner) i daje wynik

       Metody walidacyjne(rozmiar planszy (3-5), gracz(X/O))


        czekaj, ostatnia metoda ma dawac wynik na standardowe wejscie?
        metoda jest przecież showboard(), ktora ma sie aktualizowac po kazdym ruchu

     Niby tak, mozna na koniec dodac tam ifa, że koniec, jak będzie linia z X lub O, no albo koniec ruchów.
     Komunikat: X/O wygyrwa or Remis

     wklejam to na discorda bo widze, ze zaraz sesja sie tu skonczy, trzeba miec wersje ultimate zeby bezograniczen sobie korzystac z tego "live coding" z intelij
     jak cos to bedziemy na biezaco to jakos modyfikowac ale wydaje mi sie ze tutaj jzu wiekszosc napisalismy

     spoczko, ogolnie to mozna chyba zrobic kolejna sesje
     i chyba uzywac tego samego pojektu

     o to zaraz sprawdzimy bo ta sesja sie konczy niedlugo



    TODO !!!!
    walidacja danych ( jak do scanera na int wprowadzi sie inny znak sypie exception )
    wygrana po kolumnach i przekatnych


     */



    public static void main(String[] args) {

        TicTacToe ttt = new TicTacToe();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hello in Tic Tac Toe Game");
        System.out.println("Please enter board size (3-5)");

        int size = scanner.nextInt();
        scanner.nextLine();

        while (!ttt.setSize(size)) {
            System.out.println("Please enter valid board size! (3-5)");
            size = scanner.nextInt();
            scanner.nextLine();
        }

        int availableMoves = size * size; //możliwe ruchy na planszy
        int counter = 0; //ilosc ruchow

        ttt.rules();
        ttt.setBoard();

        System.out.println("Please enter player who starts (X/O)");
        char player = scanner.next().trim().toUpperCase().charAt(0);
        scanner.nextLine();

        while (!ttt.setPlayer(player)) {
            System.out.println("Please enter valid player! (X/O)");
            player = scanner.next().trim().toUpperCase().charAt(0);
            scanner.nextLine();
        }

        ttt.showBoard();

        while(counter < availableMoves) {
            System.out.println("\nEnter row coordinates: ");
            int row = scanner.nextInt(); // rozkraczy sie jak wpiszemy cos co nie jest intem trzeba by bylo obsluzyc
            scanner.nextLine();
            System.out.println("Enter column coordinates: ");
            int column = scanner.nextInt();
            scanner.nextLine();

            if(ttt.putCoordinatesOnBoard(row,column)) {
                counter++;

                if(counter >= size + (size - 1)) { //od kiedy sprawdzenie wygranej - rozmiar planszy + odpowiedz przeciwnika
                    if(ttt.checkWinByRows(ttt.getPlayer())) { //dodac po kolumnach i przekatnych w lewo i w prawo
                        System.out.println("\n\tGame over. \n\t" + ttt.getPlayer() + " WINS! \n");
                        ttt.showBoard();
                        break;
                    }
                }

                ttt.changePlayer();
            }
            else {
                System.out.println("Invalid move, try again.");
            }

            ttt.showBoard();

            if(availableMoves == counter) {
                System.out.println("\n\tGame over. \n\t" + "DRAW! \n");
                ttt.showBoard();
                break;
            }
        }
    }
}